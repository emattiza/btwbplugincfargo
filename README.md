#Introduction
This utilizes the atom functionality of BTWB to make a small plugin for script usage on a webpage
##Usage
Import the script, and an element into html at the site named appropriately.

This will pull the data through YQL, then pump out a JSON, and the appropriate styling in html and css.
##Current Features
Pulls xml data, tests for entries

##Next Features
* Needs error handling for connection issues
* Needs entry specific string building
* Needs Front-end styling
* Needs templating for plugin inclusion

