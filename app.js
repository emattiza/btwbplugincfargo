(function(){
  var feed = {
    Url: "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20xml%20where%20url%3D'http%3A%2F%2Fwww.beyondthewhiteboard.com%2Fgyms%2F3838-crossfit-argonaut.atom'&diagnostics=false&format=json",
    type: "WOD Summary",
    opType: "GET",
};

function loadFeed(feed) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            handler(xhttp);
        }
    };
    xhttp.open(feed.opType, feed.Url, true);
    xhttp.send();
}

function handler(xml) {
    var wodResults = JSON.parse(xml.response).query.results.feed;
    console.log(wodResults);
    function parseResults () {
      var finProduct = "";
      var btwbEntries = wodResults.entry;
      if(btwbEntries.length === 0){
        finProduct += "No WOD Today!";
      } else if(btwbEntries.constructor !== Array) {
        finProduct += "<p>" + btwbEntries.title + "</p><br /><p>" + btwbEntries.summary + "</p>";
      } else {
        btwbEntries.forEach(function parseEntries(curr, indx, arr) {
            if (curr.author.name == "CrossFit Argonaut") {
                finProduct += "<h4>" + curr.title + "</h4><br><p>" + curr.summary + "</p>";
            }
        });
      }
        /*for (let i = 0; i<btwbEntries.length;i++){
          if(btwbEntries[i].author.name == 'CrossFit Argonaut'){
            finProduct += "<p>" + btwbEntries[i].title + "</p><br /><p>" + btwbEntries[i].summary + "</p>";
        }
      }*/
      return finProduct;
    }

    var response = parseResults(wodResults);
    document.getElementById("btwbplugin").innerHTML = response;
}
loadFeed(feed);
})();
